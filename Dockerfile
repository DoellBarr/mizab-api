FROM python:3.10-slim

COPY . /mizab-api

RUN apt-get update && \
    apt-get install -y python3-pip libpq-dev python3-dev gcc locales git && \
    sed -i -e 's/# id_ID.UTF-8 UTF-8/id_ID.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales \
&& apt-get clean

RUN export LC_ALL="id_ID.UTF-8"
RUN export LC_CTYPE="id_ID.UTF-8"

RUN locale -a | grep id
RUN locale-gen id_ID.UTF-8

ENV LANG id_ID.UTF-8
ENV LC_ALL id_ID.UTF-8

RUN  ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/timezone && \
     ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

RUN pip3 install --upgrade pip

WORKDIR /mizab-api

RUN pip3 install --no-cache-dir -Ur requirements.txt

ENV PATH="/mizab-api/.local/bin:${PATH}"

#CMD ["uvicorn", "app.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "8080"]
#CMD ["gunicorn", "-w", "4", "-k" ,"uvicorn.workers.UvicornH11Worker", "app.main:app", "-b", "0.0.0.0:8080", "--keep-alive", "0"]
CMD ["hypercorn", "app.main:app", "--bind", "0.0.0.0:8080"]