from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from tortoise import Tortoise
from tortoise.contrib.fastapi import register_tortoise

from app.api import api_router
from app.core.configs import configs

app = FastAPI()
app.include_router(api_router)


@app.on_event("startup")
async def on_startup():
    tor = Tortoise()
    await tor.init(modules={"models": ["app"]}, db_url=configs.db_url)
    await tor.generate_schemas()
    origins = ["https://api.mizab.id/", "http://api.mizab.id/"]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
        max_age=30,
    )


register_tortoise(app, modules={"models": ["app"]}, db_url=configs.db_url)
