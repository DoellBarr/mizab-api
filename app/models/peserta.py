from tortoise import Model, fields


class AnggotaPaket(Model):
    id_anggota_paket = fields.IntField(pk=True)
    id_pembelian_paket = fields.IntField(null=True)
    nama = fields.CharField(max_length=255, null=True)
    no_hp = fields.CharField(max_length=50, null=True)
    alamat = fields.TextField(null=True)
    provinsi = fields.TextField(null=True)
    kota = fields.TextField(null=True)
    kode_pos = fields.CharField(10, null=True)
    nik = fields.CharField(max_length=25)
    foto_ktp = fields.TextField(null=True)
    npwp = fields.CharField(max_length=50, null=True)
    foto_npwp = fields.TextField(null=True)
    status_peserta = fields.SmallIntField(default=1)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    create_by = fields.IntField(null=True)
    update_by = fields.IntField(null=True)
    delete_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    delete_at = fields.DatetimeField(null=True)

    class Meta:
        table = "m_anggota_paket"

    def __str__(self):
        return self.nama


class Peserta(Model):
    id_peserta = fields.IntField(pk=True)
    id_user = fields.ForeignKeyField("models.Users", source_field="id_user")
    id_cabang = fields.ForeignKeyField(
        "models.Cabang", source_field="id_cabang"
    )
    id_pusat = fields.ForeignKeyField("models.Pusat", source_field="id_pusat")
    id_leader = fields.ForeignKeyField(
        "models.Leader", source_field="id_leader"
    )
    refferal_code = fields.CharField(max_length=255, null=True)
    nik = fields.CharField(max_length=25)
    nama = fields.CharField(max_length=255, null=True)
    email = fields.CharField(max_length=255, null=True)
    nama_ayah_kandung = fields.CharField(max_length=255, null=True)
    tempat_lahir = fields.CharField(max_length=255, null=True)
    tanggal_lahir = fields.DateField(null=True)
    kewarganegaraan = fields.CharField(max_length=255, null=True)
    alamat = fields.TextField(null=True)
    kode_pos = fields.CharField(10, null=True)
    kelurahan = fields.CharField(max_length=255, null=True)
    kecamatan = fields.CharField(max_length=255, null=True)
    rt = fields.CharField(max_length=255, null=True)
    rw = fields.CharField(max_length=255, null=True)
    provinsi = fields.TextField(null=True)
    kota = fields.TextField(null=True)
    no_hp = fields.CharField(max_length=50, null=True)
    no_tlpn = fields.CharField(max_length=50, null=True)
    ukuran_baju = fields.CharField(max_length=50, null=True)
    pendidikan_terakhir = fields.CharField(max_length=255, null=True)
    pekerjaan = fields.CharField(max_length=255, null=True)
    nama_mahram = fields.CharField(max_length=255, null=True)
    hubungan_mahram = fields.CharField(max_length=255, null=True)
    nama_keluarga = fields.CharField(max_length=255, null=True)
    no_hp_keluarga = fields.CharField(max_length=50, null=True)
    kategori_peserta = fields.CharField(max_length=255, null=True)
    passpor = fields.CharField(max_length=255, null=True)
    foto_ktp = fields.TextField(null=True)
    foto_kk = fields.TextField(null=True)
    foto_buku_nikah = fields.TextField(null=True)
    akte_lahir = fields.TextField(null=True)
    v_meningitis = fields.TextField(null=True)
    foto_3x4 = fields.TextField(null=True)
    foto_4x6 = fields.TextField(null=True)
    npwp = fields.CharField(max_length=50, null=True)
    foto_npwp = fields.TextField(null=True)
    status_peserta = fields.SmallIntField(default=1)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    created_by = fields.IntField(null=True)
    updated_by = fields.IntField(null=True)
    deleted_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    deleted_at = fields.DatetimeField(null=True)

    class Meta:
        table = "m_peserta"
