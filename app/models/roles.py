from tortoise import fields
from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise.models import Model


class Roles(Model):
    id = fields.BigIntField(pk=True, max_length=20)
    role_name = fields.CharField(max_length=255, null=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)
    deleted_at = fields.DatetimeField(null=True)
    status_aktif = fields.SmallIntField(
        default=1, description="0:Tidak Aktif; 1:Aktif;"
    )

    class Meta:
        table = "roles"

    def __str__(self):
        return self.role_name


roles_pydantic = pydantic_model_creator(Roles, name="Roles")
