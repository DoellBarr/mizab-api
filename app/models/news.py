from tortoise import Model, fields
from tortoise.contrib.pydantic import pydantic_model_creator


class Berita(Model):
    id_berita = fields.BigIntField(pk=True)
    judul = fields.CharField(max_length=255, null=True)
    link_berita = fields.CharField(max_length=255, null=True)
    deskripsi = fields.TextField(null=True)
    thumbnail = fields.CharField(max_length=255, null=True)
    publish_date = fields.DatetimeField(auto_now_add=True)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    deleted_at = fields.DatetimeField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    created_by = fields.CharField(max_length=255, null=True)
    updated_by = fields.CharField(max_length=255, null=True)

    class Meta:
        table = "m_berita"

    def __str__(self):
        return self.judul


news_pydantic = pydantic_model_creator(Berita, name="Berita", exclude=("deleted_at",))
