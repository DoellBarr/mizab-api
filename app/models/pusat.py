from datetime import datetime
from typing import Any

from pydantic import BaseModel
from tortoise import Model, fields
from tortoise.contrib.pydantic import pydantic_model_creator


class Pusat(Model):
    id_pusat = fields.IntField(pk=True)
    id_user = fields.IntField(null=True)
    nama = fields.CharField(max_length=255, null=True)
    no_hp = fields.CharField(max_length=50)
    alamat = fields.TextField(null=True)
    provinsi = fields.TextField(null=True)
    kota = fields.TextField(null=True)
    kode_pos = fields.CharField(10)
    status_aktif = fields.SmallIntField(default=1)
    created_by = fields.IntField(null=True)
    updated_by = fields.IntField(null=True)
    deleted_by = fields.IntField(null=True)
    deleted_at = fields.DatetimeField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)

    class Meta:
        table = "m_pusat"

    def __str__(self):
        return self.nama


class Cabang(Model):
    id_cabang = fields.IntField(pk=True)
    id_user = fields.ForeignKeyField(
        "models.Users",
        source_field="id_user",
        on_update=fields.CASCADE,
        on_delete=fields.CASCADE,
    )
    region = fields.CharField(max_length=255, null=True)
    nama_cabang = fields.CharField(max_length=255, null=True)
    nama = fields.CharField(max_length=255, null=True)
    no_hp = fields.CharField(max_length=50)
    alamat = fields.TextField(null=True)
    provinsi = fields.TextField(null=True)
    kota = fields.TextField(null=True)
    kode_pos = fields.CharField(10)
    status_aktif = fields.SmallIntField()
    created_by = fields.IntField(null=True)
    updated_by = fields.IntField(null=True)
    deleted_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    deleted_at = fields.DatetimeField(null=True)

    class Meta:
        table = "m_cabang"

    def __str__(self):
        return self.nama_cabang


class Leader(Model):
    id_leader = fields.IntField(pk=True)
    id_user = fields.ForeignKeyField("models.User", source_field="id_user")
    id_cabang = fields.ForeignKeyField("models.Cabang", source_field="id_cabang")
    nama_leader = fields.CharField(max_length=255, null=True)
    nik = fields.CharField(max_length=255, null=True)
    tanggal_lahir = fields.CharField(max_length=255, null=True)
    alamat = fields.TextField(null=True)
    usia = fields.SmallIntField(null=True)
    no_hp = fields.TextField(null=True)
    created_by = fields.IntField(null=True)
    updated_by = fields.IntField(null=True)
    deleted_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    deleted_at = fields.DatetimeField(null=True)

    class Meta:
        table = "m_leader"

    def __str__(self):
        return self.nama_leader


class Paket(Model):
    id_paket = fields.IntField(pk=True)
    nama_paket = fields.CharField(max_length=255, null=True)
    tujuan = fields.CharField(max_length=255, null=True)
    durasi_hari = fields.CharField(max_length=255, null=True)
    harga = fields.CharField(max_length=255, null=True)
    tanggal_berangkat = fields.DatetimeField(null=True)
    pesawat = fields.CharField(max_length=255, null=True)
    kapasitas = fields.CharField(max_length=255, null=True)
    banner = fields.TextField(null=True)
    deskripsi = fields.TextField(null=True)
    include = fields.TextField(null=True)
    exclude = fields.TextField(null=True)
    persyaratan = fields.TextField(null=True)
    timeline_perjalanan = fields.TextField(null=True)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    create_by = fields.IntField(null=True)
    update_by = fields.IntField(null=True)
    delete_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    delete_at = fields.DatetimeField(null=True)

    class Meta:
        table = "m_paket"

    def __str__(self):
        return self.nama_paket


pusat_pydantic = pydantic_model_creator(Pusat, name="Pusat")


class ModelPaket(BaseModel):
    id_paket: int
    nama_paket: str | None = None
    tujuan: str | None = None
    durasi_hari: str | None = None
    harga: str | None = None
    tanggal_berangkat: datetime | None = None
    pesawat: str | None = None
    kapasitas: str | None = None
    banner: str | None = None
    deskripsi: str | None = None
    include: str | None = None
    exclude: str | None = None
    persyaratan: str | None = None
    timeline_perjalanan: str | None = None
    status_aktif: int | None = None
    create_by: int | None = None
    update_by: int | None = None
    delete_by: int | None = None
    created_at: datetime | None = None
    updated_at: datetime | None = None
    delete_at: datetime | None = None


class Komisi(Model):
    id_komisi = fields.IntField(pk=True)
    id_penerima_komisi = fields.IntField(null=True)
    id_cabang = fields.ForeignKeyField(
        "models.Cabang", source_field="id_cabang"
    )
    id_leader = fields.ForeignKeyField(
        "models.Leader", source_field="id_leader"
    )
    id_pemberi_komisi = fields.IntField(null=True)
    total_komisi = fields.CharField(max_length=255, null=True)
    status = fields.SmallIntField(
        default=2, null=True, description="2:Tertunda;1:Pengajuan;0:Dicairkan"
    )
    bukti_transfer = fields.CharField(max_length=255, null=True)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    created_by = fields.CharField(max_length=255, null=True)
    updated_by = fields.CharField(max_length=255, null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    deleted_at = fields.DatetimeField(null=True)

    def __init__(
        self,
        *,
        id_penerima_komisi: Any,
        id_cabang: Any,
        id_leader: Any,
        id_pemberi_komisi: Any,
        total_komisi: str,
        status: int,
        bukti_transfer: str,
        status_aktif: int,
        created_by: str,
        created_at: datetime,
        updated_by: str | None = None,
        updated_at: datetime | None = None,
        deleted_at: datetime | None = None,
        **kwargs
    ):
        super().__init__(**kwargs)
        self.id_penerima_komisi = id_penerima_komisi
        self.id_cabang = id_cabang
        self.id_leader = id_leader
        self.id_pemberi_komisi = id_pemberi_komisi
        self.total_komisi = total_komisi
        self.status = status
        self.bukti_transfer = bukti_transfer
        self.status_aktif = status_aktif
        self.created_by = created_by
        self.updated_by = updated_by
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at

    class Meta:
        table = "m_komisi"

    def __str__(self):
        return self.id_penerima_komisi


class PembelianPaket(Model):
    id_pembelian_paket = fields.IntField(pk=True)
    id_paket = fields.ForeignKeyField("models.Paket", source_field="id_paket")
    id_peserta = fields.ForeignKeyField(
        "models.Peserta", source_field="id_peserta"
    )
    kategori_pembelian = fields.CharField(
        max_length=255, description="Tabungan, Langsung", null=True
    )
    no_pemesanan = fields.CharField(max_length=255, null=True)
    total_pax = fields.IntField(null=True)
    total_harga = fields.CharField(max_length=255, null=True)
    kode_voucher = fields.CharField(max_length=255, null=True)
    discount = fields.CharField(max_length=255, null=True)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    create_by = fields.IntField(null=True)
    update_by = fields.IntField(null=True)
    delete_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    delete_at = fields.DatetimeField(null=True)

    class Meta:
        table = "t_pembelian_paket"

    def __str__(self):
        return self.id_pembelian_paket


class Pembayaran(Model):
    id_pembayaran = fields.IntField(pk=True)
    id_pembelian_paket = fields.ForeignKeyField(
        "models.PembelianPaket", source_field="id_pembelian_paket"
    )
    kategori_pembayaran = fields.CharField(
        description="DP/Lunas(tabungan), Lunas(Langsung)", max_length=255
    )
    tanggal_pembayaran = fields.DatetimeField(null=True)
    kode_pembayaran = fields.CharField(max_length=255, null=True)
    total_harga = fields.CharField(max_length=255, null=True)
    bukti_pembayaran = fields.TextField(null=True)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    create_by = fields.IntField(null=True)
    update_by = fields.IntField(null=True)
    delete_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    delete_at = fields.DatetimeField(null=True)

    class Meta:
        table = "t_pembayaran"

    def __str__(self):
        return self.id_pembayaran
