from datetime import date, datetime, timedelta

from pydantic import BaseModel
from tortoise import Model, fields


class Timeline(Model):
    id_timeline_perjalanan = fields.IntField(pk=True)
    id_paket = fields.ForeignKeyField(
        "models.Paket", related_name="timeline", source_field="id_paket"
    )
    nama_kegiatan = fields.CharField(max_length=255, null=True)
    start_time = fields.TimeField(null=True)
    end_time = fields.TimeField(null=True)
    tanggal_timeline = fields.DateField(null=True)
    lokasi_kegiatan = fields.CharField(max_length=255, null=True)
    status_kegiatan = fields.SmallIntField(
        description="1:Wajib;0:Tidak Wajib", null=True
    )
    status_aktif = fields.SmallIntField(description="1:Aktif;0:Tidak Aktif", null=True)
    created_at = fields.DatetimeField(auto_now_add=True, null=True)
    updated_at = fields.DatetimeField(auto_now=True, null=True)
    delete_at = fields.DatetimeField(null=True)
    create_by = fields.IntField(null=True)
    update_by = fields.IntField(null=True)
    delete_by = fields.IntField(null=True)

    class Meta:
        table = "m_timeline"

    def __str__(self):
        return self.nama_kegiatan


class TimelineSchema(BaseModel):
    # create schema based on Timeline class
    id_timeline_perjalanan: int
    id_paket: int
    nama_kegiatan: str | None = None
    start_time: timedelta | None = None
    end_time: timedelta | None = None
    tanggal_timeline: date | None = None
    lokasi_kegiatan: str | None = None
    status_kegiatan: int | str | None = None
    status_aktif: int | None = None
    created_at: datetime | None = None
    updated_at: datetime | None = None
    delete_at: datetime | None = None
    create_by: int | None = None
    update_by: int | None = None
    delete_by: int | None = None
