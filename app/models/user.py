from tortoise import Model, fields
from tortoise.contrib.pydantic import pydantic_model_creator


class User(Model):
    id_user = fields.IntField(pk=True)
    email = fields.CharField(max_length=255, null=True)
    password = fields.CharField(max_length=500)
    role = fields.SmallIntField(default=1)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    create_by = fields.IntField(null=True)
    update_by = fields.IntField(null=True)
    delete_by = fields.IntField(null=True)
    delete_at = fields.DatetimeField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)

    class Meta:
        table = "m_user"

    def __str__(self):
        return self.email


class LogLoginUser(Model):
    id_log_login_user = fields.IntField(pk=True)
    id_user = fields.IntField(null=True)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")
    created_by = fields.IntField(null=True)
    updated_by = fields.IntField(null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)

    class Meta:
        table = "t_log_login"

    def __str__(self):
        return self.id_user


class Users(Model):
    id = fields.BigIntField(pk=True)
    role_id = fields.BigIntField(null=False)
    name = fields.CharField(max_length=255, null=False)
    email = fields.CharField(max_length=255, unique=True, null=False)
    email_verified_at = fields.DatetimeField(null=True)
    password = fields.CharField(max_length=255, null=False)
    remember_token = fields.CharField(max_length=100, null=True)
    created_at = fields.DatetimeField(null=True)
    updated_at = fields.DatetimeField(null=True)
    deleted_at = fields.DatetimeField(null=True)
    created_by = fields.IntField(null=False)
    updated_by = fields.IntField(null=True)
    deleted_by = fields.IntField(null=True)
    status_aktif = fields.SmallIntField(default=1, description="1:Aktif;0:Tidak Aktif;")

    class Meta:
        table = "users"

    def __str__(self):
        return self.name


user_pydantic = pydantic_model_creator(User, name="User")
users_pydantic = pydantic_model_creator(
    Users, name="Users", include=("id", "name", "email")
)
