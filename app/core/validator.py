from datetime import datetime, timedelta

from fastapi import HTTPException, Request, status
from fastapi.responses import JSONResponse
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from jose import jwt
from passlib.context import CryptContext

from app import Peserta
from app.core.configs import configs
from app.models import Users

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


async def authenticate_user(email: str, password: str):
    user = await Users.filter(email=email)
    if user:
        if peserta := await Peserta.get_or_none(id_user=user[0].id):
            return peserta if verify_password(password, user[0].password) else False
    return False


async def verify_user(token: str):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        payload = jwt.decode(token, configs.secret_key, algorithms=["HS256"])
    except jwt.JWTError as e:
        raise credentials_exception from e
    except Exception as e:
        raise credentials_exception from e
    else:
        user_id = payload.get("sub")
        if user_id is None:
            raise credentials_exception
        user = await Peserta.filter(id_user=user_id)
        if not user:
            raise credentials_exception
        return user[0]


def create_access_token(*, data: dict, expired_time: datetime):
    to_encode = data.copy()
    expire = datetime.now() + timedelta(seconds=expired_time.timestamp())
    to_encode["exp"] = expire
    return jwt.encode(to_encode, configs.secret_key, algorithm="HS256")


def create_refresh_token(*, data: dict):
    return jwt.encode(data, configs.secret_key)


class ValidateJWT(HTTPBearer):
    def __init__(self):
        super().__init__()

    async def __call__(self, req: Request):
        creds: HTTPAuthorizationCredentials = await super().__call__(req)
        cookies = req.cookies
        if expired := cookies.get("expired_time"):
            expired_time = datetime.fromtimestamp(int(expired))
            if expired_time < datetime.now():
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED, detail="Token expired"
                )
            if expired_time < datetime.now() + timedelta(minutes=10):
                peserta = await verify_user(cookies.get("refresh_token"))
                return await create_login(
                    peserta, cookies.get("refresh_token"), expired_time
                )
        if not creds:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Invalid authentication code.",
            )
        if creds.scheme != "Bearer":
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Invalid authentication scheme.",
            )
        if user := await verify_user(creds.credentials):
            return user


async def create_login(user: Peserta, refresh_token: str, expired_time: datetime):
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    val = await user.id_user.values()
    access_token = create_access_token(
        data={
            "sub": str(val.get("id")),
        },
        expired_time=expired_time,
    )
    response = JSONResponse(
        {
            "access_token": access_token,
            "expired_time": str(expired_time),
            "token_type": "bearer",
        }
    )
    response.set_cookie(key="access_token", value=access_token)
    response.set_cookie(key="refresh_token", value=refresh_token)
    response.set_cookie(key="expired_time", value=str(int(expired_time.timestamp())))
    return response
