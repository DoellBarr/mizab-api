from os import environ
from urllib.parse import quote_plus

from dotenv import load_dotenv

load_dotenv("app/.env")


class Configs:
    _db_user = environ.get("DB_USER")
    _db_host = environ.get("DB_HOST")
    _db_name = environ.get("DB_NAME")
    _db_pass = quote_plus(environ.get("DB_PASS"))
    _db_port = environ.get("DB_PORT")
    db_url = f"mysql://{_db_user}:{_db_pass}@{_db_host}:{_db_port}/{_db_name}"
    secret_key = environ.get("SECRET_KEY")
    url_host = environ.get("HOST")


configs = Configs()
