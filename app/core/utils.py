import base64

from aiohttp import ClientSession


async def upload(file, filename, folder):
    filee = open(file, "rb")
    encoded = base64.b64encode(filee.read())
    async with ClientSession() as s, s.post(
        "https://mizab.id/api/uploadDocument",
        data={
            "image": encoded.decode(),
            "location": folder
        }
    ) as r:
        return await r.json()
