from fastapi import APIRouter

from .api_v1 import *

api_router = APIRouter(prefix="/api/v1")
api_router.include_router(news_router, tags=["News"])
api_router.include_router(devroute, tags=["Login"])
api_router.include_router(refresh_router, tags=["Refresh"])
api_router.include_router(user_router, tags=["User"])
api_router.include_router(timeline_route, tags=["Timeline"])
api_router.include_router(foto_router, tags=["User", "Ganti Foto"])
api_router.include_router(paket_router, tags=["Paket"])
api_router.include_router(adzan_route, tags=["Adzan"])
api_router.include_router(edit_route, tags=["User", "Edit Profil"])
api_router.include_router(tf_route, tags=["Transfer"])
api_router.include_router(history_route, tags=["Riwayat Transfer"])
api_router.include_router(edit_doc_route, tags=["Edit Dokumen"])
api_router.include_router(show_doc_route, tags=["Lihat Dokumen"])
api_router.include_router(remote_route, tags=["Lihat Remote Dokumen"])
