from .endpoints import (
    adzan_route, devroute, edit_doc_route, edit_route, foto_router, history_route, news_router, paket_router,
    refresh_router, remote_route, show_doc_route, tf_route, timeline_route, user_router,
)
