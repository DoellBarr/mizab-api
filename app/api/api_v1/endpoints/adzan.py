from datetime import datetime as dt

from aiohttp import ClientSession
from fastapi import APIRouter

adzan_route = APIRouter()


@adzan_route.get("/adzan")
async def get_adzan(latitude: float | str, longitude: float | str):
    now = dt.now()
    year = now.year
    month = now.month
    async with ClientSession() as ses, ses.get(
        f"http://api.aladhan.com/v1/calendar?latitude={latitude}&longitude={longitude}&method=4&month={month}&year={year}"
    ) as resp:
        json_data = await resp.json()
        data = json_data["data"][now.day - 1]
        waktu = data["timings"]
        return generate_waktu_azan_indo(waktu)


def generate_waktu_azan_indo(data: dict[str, str]):
    subuh = data["Fajr"]
    dzuhur = data["Dhuhr"]
    ashar = data["Asr"]
    maghrib = data["Maghrib"]
    isya = data["Isha"]
    return {
        "subuh": subuh,
        "dzuhur": dzuhur,
        "ashar": ashar,
        "maghrib": maghrib,
        "isya": isya,
    }
