import random
import string
from datetime import datetime

from fastapi import APIRouter, Depends, Form, HTTPException, UploadFile
from pydantic import BaseModel

from app.core import validator
from app.core.utils import upload
from app.models import Komisi, Paket, Pembayaran, PembelianPaket, Peserta


class TransferRequestModel(BaseModel):
    id_paket: int
    jumlah_paket: int
    kategori_pembelian: int


tf_route = APIRouter()


def generate_random():
    return "PM" + "".join(random.choices(string.ascii_uppercase + string.digits, k=5))


@tf_route.post("/beli")
async def beli_paket(
    tf: TransferRequestModel, peserta: Peserta = Depends(validator.ValidateJWT())
):
    paket = await Paket.get(id_paket=tf.id_paket)
    jumlah_paket = tf.jumlah_paket
    harga = int(paket.harga) * jumlah_paket
    if peserta.id_cabang:
        id_cabang = peserta.id_cabang
        id_leader = peserta.id_leader
        id_pusat = peserta.id_pusat
        komisi_leader = 500000
        komisi_cabang = 100000
        komisi_pusat = 400000
        kom_cabang = Komisi(
            id_penerima_komisi=id_pusat,
            id_cabang=id_cabang,
            id_leader=id_leader,
            id_pemberi_komisi=peserta.id_peserta,
            total_komisi=str(komisi_cabang),
            status=2,
            bukti_transfer="",
            status_aktif=0,
            created_by=str(peserta.id_peserta),
            created_at=datetime.now(),
        )

        kom_leader = Komisi(
            id_penerima_komisi=id_pusat,
            id_cabang=peserta.id_cabang,
            id_leader=peserta.id_leader,
            id_pemberi_komisi=peserta.id_peserta,
            total_komisi=str(komisi_leader),
            status=2,
            bukti_transfer="",
            status_aktif=0,
            created_by=str(peserta.id_peserta),
            created_at=datetime.now(),
        )
        await kom_cabang.save()
        await kom_leader.save()
    elif peserta.id_leader:
        komisi_leader = 500000
        komisi_pusat = 500000
        leader = await peserta.id_leader.values()
        kom_leader = Komisi(
            id_penerima_komisi=leader.get("id_leader"),
            id_cabang=await peserta.id_cabang,
            id_leader=await peserta.id_leader,
            id_pemberi_komisi=peserta.id_peserta,
            total_komisi=str(komisi_leader),
            status=2,
            bukti_transfer="",
            status_aktif=0,
            created_by=str(peserta.id_peserta),
            created_at=datetime.now(),
        )
        await kom_leader.save()
    else:
        komisi_pusat = 1000000
    pusat = await peserta.id_pusat.values()
    kom_pusat = Komisi(
        id_penerima_komisi=pusat.get("id_pusat"),
        id_cabang=await peserta.id_cabang,
        id_leader=await peserta.id_leader,
        id_pemberi_komisi=peserta.id_peserta,
        total_komisi=str(komisi_pusat),
        status=2,
        bukti_transfer="",
        status_aktif=1,
        created_by=str(peserta.id_peserta),
        created_at=datetime.now(),
    )

    await kom_pusat.save()
    kategori_pembayaran = "Langsung" if tf.kategori_pembelian == 1 else "Tabungan"
    no_pemesanan = generate_random()
    paket = await Paket.get_or_none(id_paket=tf.id_paket)
    if not paket:
        raise HTTPException(status_code=404, detail="Paket tidak ditemukan")
    pesan = PembelianPaket(
        id_paket=paket,
        id_peserta=peserta,
        kategori_pembelian=kategori_pembayaran,
        no_pemesanan=no_pemesanan,
        total_pax=jumlah_paket,
        total_harga=str(harga),
        created_at=datetime.now(),
        status_aktif=0,
    )

    await pesan.save()
    return {"message": "Success"}


@tf_route.post("/upload/bukti", tags=["Upload Bukti"])
async def upload_bukti(
    file: UploadFile,
    id_paket: int = Form(...),
    peserta: Peserta = Depends(validator.ValidateJWT()),
):
    paket = await PembelianPaket.filter(
        id_peserta=peserta.id_peserta,
        status_aktif=0,
        id_paket=id_paket,
        kategori_pembelian="Langsung",
    )
    if not paket:
        raise HTTPException(status_code=404, detail="Pembelian Paket tidak ditemukan")
    paket = paket[-1]
    pemb = Pembayaran(
        id_pembelian_paket=paket,
        tanggal_pembayaran=datetime.now(),
        kategori_pembayaran="Lunas(Langsung)",
        kode_pembayaran=paket.no_pemesanan,
        total_harga=paket.total_harga,
        bukti_pembayaran=f"komisi/{file.filename}",
        status_aktif=0,
        create_by=str(peserta.id_peserta),
        created_at=datetime.now(),
    )
    await pemb.save()
    data = await file.read()
    await upload(data, file.filename, "komisi")
    return {"message": "Success"}


@tf_route.post("/upload/buktitabungan", tags=["Upload Bukti Tabungan"])
async def upload_bukti_tabungan(
    file: UploadFile,
    id_pembelian: int = Form(...),
    total_pembayaran: int = Form(...),
    tanggal_pembayaran: datetime = Form(datetime.now()),
    peserta: Peserta = Depends(validator.ValidateJWT()),
):
    paket = await PembelianPaket.filter(
        id_peserta=peserta.id_peserta,
        id_pembelian_paket=id_pembelian,
        kategori_pembelian="Tabungan",
    )
    if not paket:
        raise HTTPException(status_code=404, detail="Pembelian Paket tidak ditemukan")
    paket = paket[-1]
    pemb = Pembayaran(
        id_pembelian_paket=paket,
        tanggal_pembayaran=tanggal_pembayaran,
        kategori_pembayaran="Tabungan",
        kode_pembayaran=paket.no_pemesanan,
        total_harga=total_pembayaran,
        bukti_pembayaran=f"komisi/{file.filename}",
        status_aktif=0,
        create_by=str(peserta.id_peserta),
        created_at=datetime.now(),
    )
    await pemb.save()
    data = await file.read()
    await upload(data, file.filename, "komisi")
    return {"message": "Success"}
