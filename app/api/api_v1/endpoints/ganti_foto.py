import mimetypes

from fastapi import APIRouter, Depends, File, HTTPException, UploadFile, status
from pydantic import BaseModel

from app.core import validator
from app.core.utils import upload
from app.models.user import Users


class ChangePhotoSuccessResponse(BaseModel):
    detail: str


class ChangePhotoFailedResponse(BaseModel):
    detail: str


foto_router = APIRouter()


@foto_router.put(
    "/user/foto",
    response_model=ChangePhotoSuccessResponse,
    responses={415: {"model": ChangePhotoFailedResponse}},
)
async def update_foto(
    user: Users = Depends(validator.ValidateJWT()), foto: UploadFile = File(...)
):
    # TODO: BUG
    file_name = foto.filename.lower()
    content_type = mimetypes.guess_type(file_name)[0]
    if content_type not in ["image/jpeg", "image/png"]:
        raise HTTPException(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            detail="File type not supported",
        )
    user.foto_3x4 = await foto.read()
    await upload(await foto.read(), file_name, "jamaah")
    await user.update_from_dict({"foto_3x4": user.foto_3x4})
    return {"detail": "Foto berhasil diupdate"}
