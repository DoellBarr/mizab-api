from fastapi import APIRouter, Depends
from fastapi.responses import StreamingResponse

from app.core import validator

remote_route = APIRouter()


@remote_route.get("/uploads/{file_path:path}", dependencies=[Depends(validator.ValidateJWT())])
async def showing_photo(file_path: str):
    return StreamingResponse(open(f"/mnt/storage/public/uploads/{file_path}", "rb"), media_type="image/png")
