from datetime import datetime

from fastapi import APIRouter, Request

from app.core import validator

refresh_router = APIRouter()


@refresh_router.post("/refresh")
async def refresh(req: Request):
    refresh_token = req.cookies.get("refresh_token")
    expired_time = int(req.cookies.get("expired_time"))
    peserta = await validator.verify_user(refresh_token)
    return await validator.create_login(
        peserta, refresh_token, datetime.fromtimestamp(expired_time)
    )
