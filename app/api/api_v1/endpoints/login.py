from datetime import datetime, timedelta

from fastapi import APIRouter, Form, HTTPException
from pydantic import BaseModel

from app import Peserta
from app.core.validator import authenticate_user, create_login, create_refresh_token


class LoginResponse(BaseModel):
    access_token: str
    expired_time: datetime
    token_type: str


class LoginFailedResponse(BaseModel):
    detail: str


class LoginModel(BaseModel):
    email: str = Form()
    password: str = Form()
    expired_time: datetime | None = Form()


devroute = APIRouter()


@devroute.post(
    "/login",
    response_model=LoginResponse,
    responses={401: {"model": LoginFailedResponse}},
)
async def login(form_data: LoginModel):
    form_data.expired_time = datetime.now() + timedelta(days=3)
    user: Peserta = await authenticate_user(form_data.email, form_data.password)
    if not user:
        raise HTTPException(status_code=401, detail="Incorrect email or password")
    refresh_token = create_refresh_token(data={"sub": str(user.id_user)})
    return await create_login(user, refresh_token, form_data.expired_time)
