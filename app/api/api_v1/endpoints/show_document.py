from urllib.parse import quote

from fastapi import APIRouter, Depends
from pydantic import BaseModel

from app.core.configs import configs
from app.core.validator import ValidateJWT
from app.models import Peserta


class ShowDocResponse(BaseModel):
    passpor: str | None = None
    ktp: str | None = None
    kk: str | None = None
    bukunikah: str | None = None
    akta: str | None = None
    v_meningitis: str | None = None
    foto_3x4: str | None = None
    foto_4x6: str | None = None
    foto_npwp: str | None = None


show_doc_route = APIRouter()


@show_doc_route.get("/show_doc", response_model=ShowDocResponse)
async def get_show_doc(peserta: Peserta = Depends(ValidateJWT())):
    passpor = f"{configs.url_host}/uploads/jamaah/{quote(peserta.passpor or '')}"
    ktp = f"{configs.url_host}/uploads/jamaah/{quote(peserta.foto_ktp or '')}"
    kk = f"{configs.url_host}/uploads/jamaah/{quote(peserta.foto_kk or '')}"
    bukunikah = f"{configs.url_host}/uploads/jamaah/{quote(peserta.foto_buku_nikah or '')}"
    akta = f"{configs.url_host}/uploads/jamaah/{quote(peserta.akte_lahir or '')}"
    v_meningitis = f"{configs.url_host}/uploads/jamaah/{quote(peserta.v_meningitis or '')}"
    foto_3x4 = f"{configs.url_host}/uploads/jamaah/{quote(peserta.foto_3x4) or ''}"
    foto_4x6 = f"{configs.url_host}/uploads/jamaah/{quote(peserta.foto_4x6) or ''}"
    foto_npwp = f"{configs.url_host}/uploads/jamaah/{quote(peserta.foto_npwp or '')}"
    return {
        "passpor": passpor,
        "ktp": ktp,
        "kk": kk,
        "bukunikah": bukunikah,
        "akta": akta,
        "v_meningitis": v_meningitis,
        "foto_3x4": foto_3x4,
        "foto_4x6": foto_4x6,
        "foto_npwp": foto_npwp,
    }
