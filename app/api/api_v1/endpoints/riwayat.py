from fastapi import APIRouter, Depends

from app.core.validator import ValidateJWT
from app.models import Pembayaran, PembelianPaket, Peserta

history_route = APIRouter()


@history_route.get("/history")
async def get_transfer_history(peserta: Peserta = Depends(ValidateJWT())):
    if not (
        pembelian_paket := await PembelianPaket.filter(id_peserta=peserta.id_peserta)
    ):
        return []
    datas = []
    for p in pembelian_paket:
        paket = await p.id_paket.values()
        peser = await p.id_peserta.values()
        kategori = 1 if p.kategori_pembelian == "Langsung" else 0
        x = {
            "id_pembelian_paket": p.id_pembelian_paket,
            "id_paket": paket.get("id_paket"),
            "id_peserta": peser.get("id_peserta"),
            "kategori_pembelian": kategori,
            "no_pemesanan": p.no_pemesanan,
            "nama_paket": paket.get("nama_paket"),
            "harga_paket": p.total_harga,
            "jumlah_pax": p.total_pax,
            "status_aktif": p.status_aktif,
        }

        datas.append(x)
    return datas


@history_route.get("/historytabungan")
async def get_tabungan_history(
    id_pembelian_paket: int, peserta: Peserta = Depends(ValidateJWT())
):
    if pembelian_paket := await PembelianPaket.filter(
        id_peserta=peserta.id_peserta,
        kategori_pembelian="Tabungan",
        id_pembelian_paket=id_pembelian_paket,
    ):
        datas = []
        for p in pembelian_paket:
            paket = await p.id_paket.values()
            pembayaran = await Pembayaran.filter(
                id_pembelian_paket=p.id_pembelian_paket
            )
            x = {
                "id_paket": paket.get("id_paket"),
                "nama_paket": paket.get("nama_paket"),
                "detail": [],
                "id_pembelian_paket": p.id_pembelian_paket,
            }
            for pem in pembayaran:
                x["detail"].append(
                    {
                        "total_bayar": pem.total_harga,
                        "tanggal_bayar": pem.tanggal_pembayaran,
                    }
                )
            datas.append(x)
        return datas
    return []
