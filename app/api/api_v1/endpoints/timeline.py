from datetime import date
from typing import List

from fastapi import APIRouter, Depends

from app import models
from app.core.validator import ValidateJWT

timeline_route = APIRouter()


@timeline_route.get(
    "/timeline",
    response_model=List[models.timeline.TimelineSchema],
    dependencies=[Depends(ValidateJWT())],
)
async def get_timelines(tanggal: date = date.today()):
    datas = await models.Timeline.filter(status_aktif=1, tanggal_timeline=tanggal)
    new_datas = []
    for data in datas:
        val = await data.id_paket.values()
        new_data = {
            "id_timeline_perjalanan": data.id_timeline_perjalanan,
            "id_paket": val.get("id_paket"),
            "nama_kegiatan": data.nama_kegiatan,
            "start_time": data.start_time,
            "end_time": data.end_time,
            "tanggal_timeline": data.tanggal_timeline,
            "lokasi_kegiatan": data.lokasi_kegiatan,
            "status_kegiatan": "Wajib" if data.status_kegiatan == 1 else "Tidak Wajib",
            "status_aktif": data.status_aktif,
            "created_at": data.created_at,
            "updated_at": data.updated_at,
            "delete_at": data.delete_at,
            "create_by": data.create_by,
            "update_by": data.update_by,
            "delete_by": data.delete_by,
        }
        new_datas.append(new_data)
    return new_datas
