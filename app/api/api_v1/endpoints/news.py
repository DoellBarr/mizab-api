from urllib.parse import quote_plus
from app.core.configs import configs
from app.models.news import Berita, news_pydantic
from app.core import validator
from typing import List
from fastapi import APIRouter, Depends

news_router = APIRouter()


@news_router.get(
    "/news",
    response_model=List[news_pydantic],
    dependencies=[Depends(validator.ValidateJWT())],
)
async def get_news():
    news = await Berita.filter(status_aktif=1)
    datas = []
    for new in news:
        data = {
            "id_berita": new.id_berita,
            "judul": new.judul,
            "link_berita": new.link_berita,
            "deskripsi": new.deskripsi,
            "thumbnail": f"{configs.url_host}/uploads/{quote_plus(new.thumbnail)}",
            "publish_date": new.publish_date,
            "status_aktif": new.status_aktif,
            "created_at": new.created_at,
            "updated_at": new.updated_at,
            "created_by": new.created_by,
            "updated_by": new.updated_by,
        }
        datas.append(data)
    return datas
