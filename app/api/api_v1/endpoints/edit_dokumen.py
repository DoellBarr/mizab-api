from fastapi import APIRouter, Depends, UploadFile
from pydantic import BaseModel

from app.core.utils import upload
from app.core.validator import ValidateJWT
from app.models import Peserta

edit_doc_route = APIRouter()


class EditDokumenResponse(BaseModel):
    message: str


@edit_doc_route.post("/upload/paspor", response_model=EditDokumenResponse)
async def upload_paspor(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.passpor = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "Passpor berhasil diganti",
    }


@edit_doc_route.post("/upload/ktp", response_model=EditDokumenResponse)
async def upload_ktp(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.foto_ktp = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "KTP berhasil diganti",
    }


@edit_doc_route.post("/upload/kk", response_model=EditDokumenResponse)
async def upload_kk(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.foto_kk = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "KK berhasil diganti",
    }


@edit_doc_route.post("/upload/bukunikah", response_model=EditDokumenResponse)
async def upload_bukunikah(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.foto_buku_nikah = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "Buku Nikah berhasil diganti",
    }


@edit_doc_route.post("/upload/akta", response_model=EditDokumenResponse)
async def upload_akta(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.akte_lahir = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "Akta berhasil diganti",
    }


@edit_doc_route.post("/upload/meningitis", response_model=EditDokumenResponse)
async def upload_meningitis(
    file: UploadFile, peserta: Peserta = Depends(ValidateJWT())
):
    peserta.v_meningitis = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "Meningitis berhasil diganti",
    }


@edit_doc_route.post("/upload/foto3", response_model=EditDokumenResponse)
async def upload_foto3(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.foto_3x4 = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "Foto 3x4 berhasil diganti",
    }


@edit_doc_route.post("/upload/foto4", response_model=EditDokumenResponse)
async def upload_foto4(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.foto_4x6 = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "Foto 4x6 berhasil diganti",
    }


@edit_doc_route.post("/upload/npwp", response_model=EditDokumenResponse)
async def upload_npwp(file: UploadFile, peserta: Peserta = Depends(ValidateJWT())):
    peserta.foto_npwp = f"jamaah/{file.filename}"
    await peserta.save()
    await upload(await file.read(), file.filename, "jamaah")
    return {
        "message": "NPWP berhasil diganti",
    }
