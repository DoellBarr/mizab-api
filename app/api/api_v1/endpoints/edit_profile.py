from datetime import date

from fastapi import APIRouter, Depends, Form
from pydantic import BaseModel

from app import Peserta
from app.core import validator

edit_route = APIRouter()


class PesertaEditModel(BaseModel):
    nama_lengkap: str | None = Form()
    email: str | None = Form()
    tanggal_lahir: str | None = Form()
    asal_daerah: str | None = Form()
    no_hp: str | None = Form()


class PesertaResponseModel(BaseModel):
    nama: str | None = None
    tanggal_lahir: date | None = None
    kota: str | None = None
    no_hp: str | None = None


@edit_route.put("/user", response_model=PesertaResponseModel)
async def edit_user(
    peserta_edit: PesertaEditModel,
    peserta: Peserta = Depends(validator.ValidateJWT()),
):
    #
    if peserta_edit.nama_lengkap:
        peserta.nama = peserta_edit.nama_lengkap
    if peserta_edit.tanggal_lahir:
        peserta.tanggal_lahir = peserta_edit.tanggal_lahir
    if peserta_edit.asal_daerah:
        peserta.kota = peserta_edit.asal_daerah
    if peserta_edit.no_hp:
        peserta.no_hp = peserta_edit.no_hp
    await peserta.save()
    return PesertaResponseModel(**peserta.__dict__)
