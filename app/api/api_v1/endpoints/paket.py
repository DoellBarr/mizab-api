from typing import List
from urllib.parse import quote

from fastapi import APIRouter, Depends

from app.core import validator
from app.core.configs import configs
from app.models.pusat import ModelPaket as paket_pydantic, Paket

paket_router = APIRouter()


@paket_router.get(
    "/paket",
    response_model=List[paket_pydantic],
    dependencies=[Depends(validator.ValidateJWT())],
)
async def get_paket():
    pakets = await Paket.filter(status_aktif=1)
    datas = []
    for paket in pakets:
        if paket.banner:
            paket.banner = f"{configs.url_host}/uploads/{quote(paket.banner)}"
        datas.append(paket_pydantic(**paket.__dict__))
    return datas
