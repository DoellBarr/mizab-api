from urllib.parse import quote

from fastapi import APIRouter, Depends
from pydantic import BaseModel

from app import Paket, PembelianPaket, Peserta
from app.core import validator
from app.core.configs import configs


class PesertaResponseModel(BaseModel):
    nama_lengkap: str
    email: str | None = None
    ttl: str
    asal_daerah: str | None = None
    no_hp: str
    foto: str | None = None
    paket: dict[str, str] | None = None


user_router = APIRouter()


@user_router.get("/user", response_model=PesertaResponseModel)
async def get_users(peserta: Peserta = Depends(validator.ValidateJWT())):
    object_paket = {}
    if beli_paket := await PembelianPaket.filter(id_peserta=peserta.id_peserta):
        pket = await Paket.get(
            id_paket=(await beli_paket[0].id_paket.values()).get("id_paket")
        )
        if pket:
            dur = str(pket.durasi_hari).split("-")
            mulai = dur[0].strip()
            selesai = dur[1].strip()
            object_paket = {"tanggal_mulai": mulai, "tanggal_selesai": selesai}
    ttl = f"{peserta.tempat_lahir}, {peserta.tanggal_lahir}"
    data = {
        "nama_lengkap": peserta.nama,
        "email": peserta.email,
        "ttl": ttl,
        "asal_daerah": peserta.kota,
        "no_hp": peserta.no_hp,
        "foto": f"{configs.url_host}/uploads/{quote(peserta.foto_3x4)}",
        "paket": object_paket,
    }
    return PesertaResponseModel(**data)
